#! /usr/bin/env python
from __future__ import print_function

"""
BWA-Mem Alignment to reference genome.
Version: 1.0.0
"""
import sys
import os


def main():
    bwa_index = sys.argv[1]
    forward_reads = sys.argv[2]
    reverse_reads = sys.argv[3]

    try:
        file = open("read_group", "r")
        read_group = "'" + str(file.read()).strip() + "'"
        print(read_group)
        file.close()
    except Exception as e:
        print('Error while executing opening file %s' % e)
        sys.exit(1)

    try:
        # command = "run-bwamem -o out.aln.bam -t 4 -R " + read_group + " -H " + \
        #           bwa_index + " " + \
        #           forward_reads + " " + \
        #           reverse_reads + " | sh"
        command = "seqtk mergepe " + forward_reads + " " + reverse_reads + " > out.fq"
        print(command)
        os.system(command)

        command = "bwa mem -p -t4 -R " + read_group + " " + bwa_index + " out.fq -o out.sam - 2> out.aln.bam.log.bwamem"
        print(command)
        os.system(command)

        command = "samtools view -1 -S -b out.sam > aln.bam"
        print(command)
        os.system(command)
    except Exception as e:
        print('Error while executing run-bwamem -> %s %s' % command % e)
        sys.exit(1)


if __name__ == '__main__':
    main()
    sys.exit(0)
