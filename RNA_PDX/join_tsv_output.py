#! /usr/bin/env python3
"""
For generating single output table to export to DataHub.
Version: 1.1.0
"""

import sys

import numpy as np
import pandas


def linear_dataset(df1, column_name, count):
    result_file_linear = "joined_results_linear"

    if count == 0:
        df1.to_csv(result_file_linear, sep='\t', index=False)
        print(df1.head())
    else:
        df2 = pandas.read_csv(result_file_linear, sep='\t', lineterminator='\n')
        df_linear = pandas.merge(df1, df2, left_on=['GeneName', 'gene_id'], right_on=['GeneName', 'gene_id'],
                                 how='outer',
                                 suffixes=('_left', '_right'))
        df_linear.to_csv(result_file_linear, sep='\t', index=False)
        print(df_linear.head())


def log2_dataset(df1, column_name, count):
    result_file_log2 = "joined_results_log2"

    if count == 0:
        df1['log2_value'] = np.log2(df1[column_name])
        df2 = df1.drop(column_name, 1)
        df2.rename(columns={'log2_value': column_name}, inplace=True)
        df2.to_csv(result_file_log2, sep='\t', index=False)
        print(df2.head())
    else:
        df1['log2_value'] = np.log2(df1[column_name])
        df3 = df1.drop(column_name, 1)
        df3.rename(columns={'log2_value': column_name}, inplace=True)
        df4 = pandas.read_csv(result_file_log2, sep='\t', lineterminator='\n')
        df_log2 = pandas.merge(df3, df4, left_on=['GeneName', 'gene_id'], right_on=['GeneName', 'gene_id'],
                               how='outer',
                               suffixes=('_left', '_right'))
        df_log2.to_csv(result_file_log2, sep='\t', index=False)
        print(df_log2.head())


def zscore_dataset(df_initial, column_name, count):
    result_file_zscore = "joined_results_zscore"

    if count == 0:
        df_initial['zscore'] = (df_initial[column_name] - df_initial[column_name].mean()) / df_initial[
            column_name].std(ddof=0)
        df3 = df_initial.drop(column_name, 1)
        df3.rename(columns={'zscore': column_name}, inplace=True)
        df3.to_csv(result_file_zscore, sep='\t', index=False)
        print(df3.head())
    else:
        df_initial['zscore'] = (df_initial[column_name] - df_initial[column_name].mean()) / df_initial[
            column_name].std(ddof=0)
        df3 = df_initial.drop(column_name, 1)
        df3.rename(columns={'zscore': column_name}, inplace=True)
        df4 = pandas.read_csv(result_file_zscore, sep='\t', lineterminator='\n')
        df_zscore = pandas.merge(df3, df4, left_on=['GeneName', 'gene_id'], right_on=['GeneName', 'gene_id'],
                                 how='outer',
                                 suffixes=('_left', '_right'))
        df_zscore.to_csv(result_file_zscore, sep='\t', index=False)
        print(df_zscore.head())


def merged_dataset(df1, column_name, count):
    result_file = "joined_results_merged"

    df1 = df1.drop('gene_id', 1)
    df1['sample_id'] = column_name
    df1.rename(columns={column_name: "rnaseq_count", "GeneName": "symbol"}, inplace=True)

    # Remove the header names from df except first df
    if count == 0:
        df1.to_csv(result_file, sep='\t', mode='a', index=False)
    else:
        df1.to_csv(result_file, sep='\t', mode='a', header=False, index=False)


def main():
    output_dataset = sys.argv[1]
    list_of_dataset = sys.argv[2:]
    count = 0

    for dataset_name in list_of_dataset:
        print("[INFO] Processing Dataset: " + dataset_name)
        try:
            df1 = pandas.read_csv(dataset_name, sep='\t', lineterminator='\n')
            column_name = df1.columns[2]
            print(f'[INFO] {column_name}')

            df_initial = df1.copy()
            if output_dataset == "linear":
                linear_dataset(df1, column_name, count)

            if output_dataset == "log2":
                log2_dataset(df1, column_name, count)

            if output_dataset == "zscore":
                zscore_dataset(df_initial, column_name, count)

            if output_dataset == "all":
                linear_dataset(df1, column_name, count)
                log2_dataset(df1, column_name, count)
                zscore_dataset(df_initial, column_name, count)

            if output_dataset == "merged":
                merged_dataset(df1, column_name, count)

            count = 1
        except Exception as e:
            print('[ERROR] Processing data in join_tsv_output tool -> %s' % e)
            sys.exit(1)


if __name__ == '__main__':
    main()
