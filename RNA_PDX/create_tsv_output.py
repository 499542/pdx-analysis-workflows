#! /usr/bin/env python3
"""
For generating filtered tsv output for each sample.
Version: 1.0.0
"""

import sys
import pandas


def main():
    dataset_name = sys.argv[1]
    sample_name = sys.argv[2]
    metric_column = sys.argv[3]
    result_file = "created_results_tsv"

    try:
        df1 = pandas.read_csv(dataset_name, sep='\t', lineterminator='\n')
        df1 = df1[['GeneName', 'gene_id', metric_column]]
        df1.rename(columns={metric_column: sample_name}, inplace=True)
        print(df1.head())
        columns_list = ["GeneName", "gene_id", sample_name]
        df1.to_csv(result_file, sep='\t', index=False, columns=columns_list)
    except Exception as e:
        print('Error while processing data in create_tsv_output tool -> %s' % e)
        sys.exit(1)


if __name__ == '__main__':
    main()
